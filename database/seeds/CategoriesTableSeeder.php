<?php

use Illuminate\Database\Seeder;
use App\Category;

class CategoriesTableSeeder extends Seeder

{
    /**
     * Run the database seeds.
     *
     * @return void
     */

    private $x = ['uncategorized', 'reviews', 'bonus'];

    public function run()
    {
        for ($i = 0; $i < count($this->x); $i++) {
            Category::create([
                'name' => $this->x[$i]
            ]);
        }
    }
}
