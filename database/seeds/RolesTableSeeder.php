<?php

use Illuminate\Database\Seeder;
use App\Role;

class RolesTableSeeder extends Seeder
{
    private $x = ['player', 'moderator', 'author', 'administrator', 'superuser'];

    public function run()
    {
        for ($i = 0; $i < count($this->x); $i++) {
            Role::create([
                'name' => $this->x[$i]
            ]);
        }
    }
}
