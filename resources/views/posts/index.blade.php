@extends('layouts.app')

@section('title', 'Profile')

@section('content')

<header id="section-header-home">

</header>

<section id="section-home-1">

    <div class="container-fluid">
        <div class="row">
            <div class="col-lg-6">
                <div class="row">
                    <div class="col d-flex justify-content-center">
                        
                    </div>
                </div>
            </div>
            <div class="col-lg-6">
                @if(count($posts) > 0)
                <div class="row">
                    @foreach($posts as $post)
                    <div class="col-6">
                        <div class="card" style="width:;">
                            <img class="card-img-top" src="{{asset('images/' . $post->image)}}" alt="Card image cap">
                            <div class="card-body">
                                <h5 class="card-title"><a href="posts/{{$post->id}}">{{$post->title}}</a></h5>
                                <p class="card-text">{{$post->body}}</p>
                                <small class="card-text">By: {{$post->user->name}}</small>
                                <br>
                                <small class="card-text">Email: {{$post->user->email}}</small>
                                <p><small class="card-text">Created On: {{$post->created_at}}</small></p>
                                <a href="posts/{{$post->id}}" class="btn btn-primary">View Post</a>
                                @auth
                                <a href="/posts/{{$post->id}}/edit" class="btn btn-danger">Edit</a>
                                <!-- Button trigger modal -->
                                <button type="button" class="btn btn-danger" data-toggle="modal" data-target="#exampleModal">
                                    Delete
                                </button>
                                <!-- Modal -->
                                <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
                                    aria-hidden="true">
                                    <div class="modal-dialog" role="document">
                                        <div class="modal-content">
                                            <div class="modal-header">
                                                <h5 class="modal-title" id="exampleModalLabel">Do you really want to delete
                                                    this item?</h5>
                                            </div>
                                            <div class="modal-footer">
                                            <form method="POST" action="posts/{{$post->id}}">
                                                    @method('DELETE')
                                                    @csrf
                                                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancel</button>
                                                    <button type="submit" class="btn btn-danger">Delete</button>
                                                </form>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                @endauth
                                <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
                                    aria-hidden="true">
                                    <div class="modal-dialog" role="document">
                                        <div class="modal-content">
                                            <div class="modal-header">
                                                <h5 class="modal-title" id="exampleModalLabel">Modal title</h5>
                                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                    <span aria-hidden="true">&times;</span>
                                                </button>
                                            </div>
                                            <div class="modal-body">
                                                ...
                                            </div>
                                            <div class="modal-footer">
                                                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                                                <button type="button" class="btn btn-primary">Save changes</button>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                            </div>
                        </div>
                    </div>
                    @endforeach
                    {{$posts->links()}}
                </div>
                @else
            </div>    
            <h3>There is no post at the moment.</h3>
            @endif
        </div>
    </div>

</section>

@endsection
