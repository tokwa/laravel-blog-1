@extends('layouts.app')

@section('content')

<head id="section-header-create">

</head>

<section id="section-create-1">

<div class="container">
    <form method="POST" action="{{route('posts.store')}}" enctype="multipart/form-data">
        {{ csrf_field() }}
            <div class="form-group">
            <label for="title">Post Title</label>
            <input type="text" class="form-control {{ $errors->has('title') ? 'is-invalid' : '' }}" id="title" name="title" aria-describedby="title-Help" placeholder="Post title..." value="{{old('title')}}">
            <small id="titleHelp" class="form-text text-muted">Title of the post.</small>
                @if ($errors->has('title'))
                <span class="invalid-feedback" role="alert">
                    <strong>{{ $errors->first('title') }}</strong>
                </span>
                @endif
            </div>
            <div class="form-group">
                <label for="body">Post Body</label>
            <textarea class="form-control my-editor {{ $errors->has('body') ? 'is-invalid' : '' }}" id="body" name="body" rows="3">{{old('body')}}</textarea>
                <small id="titleHelp" class="form-text text-muted">Post content.</small>
                @if ($errors->has('body'))
                <span class="invalid-feedback" role="alert">
                    <strong>{{ $errors->first('body') }}</strong>
                </span>
                @endif
            </div>
            <div class="form-group">
                <input type="file" name="image" id="image"/>
            </div>
            {{-- <div class="custom-control custom-checkbox custom-control-inline">
                @if (count($categories))
                    @foreach($categories as $category)
                    <div class="custom-control custom-checkbox custom-control-inline">
                        <input type="checkbox" name="category[]" class="custom-control-input text-lowercase row_check
                                {{ $errors->has('category') ? 'is-invalid' : '' }}"
                                id="{{ "category-" . utf8_decode($category->name) }}" value="{{ $category->id }}" 
                                {{ $loop->first ? 'checked' : '' }}
                                {{ in_array($category->id, old('category', [])) ? 'checked' : '' }}>
                        <label class="custom-control-label text-lowercase" for="{{ "category-". utf8_decode($category->name) }}">{{
                                utf8_decode($category->name) }}</label>
                    </div>
                    @endforeach
                @else
                    <p>
                        There is no category at the moment.
                    </p>
                @endif
            </div> --}}
            <div class="form-group row">
                    {{-- POST TAGS --}}
                    <div class="col-lg-12">
                        <strong class="text-muted mb-3 mt-4 d-block">Tags:</strong>
    
                        @if( !count($categories) )
                        <h6>No categories found.</h6>
                        @else
                        <div class="custom-control custom-checkbox custom-control-inline">
                            <input type="checkbox" id="select_all" class="custom-control-input text-lowercase">
                            <label class="custom-control-label text-uppercase" for="select_all"><strong>select all</strong></label>
                        </div>
                        @foreach( $categories as $category )
                        <div class="custom-control custom-checkbox custom-control-inline">
                            <input type="checkbox" name="categories[]" class="custom-control-input text-lowercase row_check
                                {{ $errors->has('category') ? 'is-invalid' : '' }}" 
                                id="{{ "category-" . utf8_decode($category->name) }}" value="{{ $category->id }}" 
                                {{ $loop->first ? 'checked' : '' }}
                                {{ in_array($category->id, old('category', [])) ? 'checked' : '' }}>
                            <label class="custom-control-label text-lowercase" for="{{ "category-". utf8_decode($category->name) }}">
                            {{ utf8_decode($category->name) }}</label>
                        </div>
                        @endforeach

                        @if( $errors->has('categories') )
                        <small id="{{ strtolower(utf8_decode($category->name)) }}" class="form-text text-danger">
                            <strong>{{ $errors->first('categories') }}</strong>
                        </small>
                        @endif

                        @endif
                    </div>
                </div>
            <div class="form-group">
            <a class="btn btn-danger" href="{{URL::previous()}}">Cancel</a>
                <button type="submit" class="btn btn-primary">
                        {{ __('Submit') }}
                </button>
            </div>
    </form>
</div>
</section>



<script src="//cdn.tinymce.com/4/tinymce.min.js"></script>
{{-- <textarea name="content" class="form-control my-editor">{!! old('content', $content) !!}</textarea> --}}
<script>
  var editor_config = {
    path_absolute : "/",
    selector: "textarea.my-editor",
    plugins: [
      "advlist autolink lists link image charmap print preview hr anchor pagebreak",
      "searchreplace wordcount visualblocks visualchars code fullscreen",
      "insertdatetime media nonbreaking save table contextmenu directionality",
      "emoticons template paste textcolor colorpicker textpattern"
    ],
    toolbar: "insertfile undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image media | forecolor backcolor",
    relative_urls: false,
    file_browser_callback : function(field_name, url, type, win) {
      var x = window.innerWidth || document.documentElement.clientWidth || document.getElementsByTagName('body')[0].clientWidth;
      var y = window.innerHeight|| document.documentElement.clientHeight|| document.getElementsByTagName('body')[0].clientHeight;

      var cmsURL = editor_config.path_absolute + 'laravel-filemanager?field_name=' + field_name;
      if (type == 'image') {
        cmsURL = cmsURL + "&type=Images";
      } else {
        cmsURL = cmsURL + "&type=Files";
      }

      tinyMCE.activeEditor.windowManager.open({
        file : cmsURL,
        title : 'Filemanager',
        width : x * 0.8,
        height : y * 0.8,
        resizable : "yes",
        close_previous : "no"
      });
    }
  };

  tinymce.init(editor_config);

    $('#select_all').on('click', (e) => {
            $('.row_check').prop('checked', $(e.target).prop('checked'));
        })

        $('.row_check').on('click', () => {
            if ($('.row_check:checked').length == $('.row_check').length) {
                $('#select_all').prop('checked', true)
            } else {
                $('#select_all').prop('checked', false)
            }
    })
</script>
@endsection