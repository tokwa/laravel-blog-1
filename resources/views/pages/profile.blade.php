{{-- @extends('layouts.app')

@section('title', 'Profile')

@section('content')
Name: {{$user->name}}
<br>
Email: {{$user->email}}

<br><br>

@foreach($user->posts as $post)
Title: {{$post->title}}
<br>
Body: {{$post->body}}
@endforeach

@endsection --}}


@extends('layouts.app')

@section('title', 'Profile')

@section('content')

<header id="section-header-home">

</header>

<section id="section-home-1">

    <div class="container-fluid">
        <div class="row">
            <div class="col-lg-6">
                <div class="row">
                    <div class="col-12 d-flex justify-content-center">
                        <img src="{{asset('image/' . $username->image)}}" alt="{{$username->name}}" id="profile-img">
                    </div>
                    <div class="col-12 d-flex justify-content-center mt-3">
                        <h2>{{$username->username}}</h2>
                    </div>
                </div>
                <div class="row d-flex justify-content-center mt-5">
                    <div class="col-12-lg">
                    <form method="POST" action="{{ route('profile.update', $username->username) }}">
                        @csrf
                        @method('PUT')
                            <div class="form-group">
                                <div class="row">
                                    <div class="col-3">
                                        <label for="exampleInputEmail1">Name: </label>
                                    </div>
                                    <div class="col-9">
                                        <input type="text" class="form-control{{ $errors->has('name') ? ' is-invalid' : '' }}"
                                            name="name" id="name" aria-describedby="name" value="{{$username->name}}">

                                            @if ($errors->has('name'))
                                            <span class="invalid-feedback" role="alert">
                                                <strong>{{ $errors->first('name') }}</strong>
                                            </span>
                                            @endif
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="row">
                                    <div class="col-3">
                                        <label for="exampleInputEmail1">Email: </label>
                                    </div>
                                    <div class="col-9">
                                        <input id="email" type="email" class="form-control{{ $errors->has('email') ? ' is-invalid' : '' }}"
                                    name="email" value="{{$username->email}}" required>

                                            @if ($errors->has('email'))
                                            <span class="invalid-feedback" role="alert">
                                                <strong>{{ $errors->first('email') }}</strong>
                                            </span>
                                            @endif
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="row">
                                    <div class="col-3">
                                        <label for="exampleInputPassword1">Password:</label>
                                    </div>
                                    <div class="col-9">
                                        <input type="password" class="form-control" id="exampleInputPassword1"
                                            placeholder="Password">
                                    </div>
                                </div>
                            </div>
                            <button type="submit" class="btn btn-primary">Submit</button>
                        </form>
                    </div>
                </div>
            </div>
            <div class="col-lg-6">
                @if(count($username->posts) > 0)
                <div class="row">
                    @foreach($username->posts as $post)
                    <div class="col-6">
                        <div class="card" style="width:;">
                            <img class="card-img-top" src="{{asset('images/' . $post->image)}}" alt="Card image cap">
                            <div class="card-body">
                                <h5 class="card-title"><a href="posts/{{$post->id}}">{{$post->title}}</a></h5>
                                <p class="card-text">{{$post->body}}</p>
                                <small class="card-text">By: {{$post->user->name}}</small>
                                <br>
                                <small class="card-text">Email: {{$post->user->email}}</small>
                                <p><small class="card-text">Created On: {{$post->created_at}}</small></p>
                                <a href="/{{$post->id}}" class="btn btn-primary">View Post</a>
                            </div>
                        </div>
                    </div>
                    @endforeach
                </div>
                @else
            </div>
            <h3>You do not have a post at the moment.</h3>
            @endif
        </div>
    </div>

</section>

@endsection
