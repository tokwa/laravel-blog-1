@extends('layouts.app')

@section('title', 'Home')

@section('content')

<header id="section-header-home">

</header>

<section id="section-home-1">
    <div class="container">
        @if(count($posts) > 0)
        <div class="row">
            @foreach($posts as $post)
            <div class="col-lg-4 mb-3">
                <div class="card" style="width:;">
                    <img class="card-img-top p-img-container" src="{{asset('images/' . $post->image)}}" alt="Card image cap">
                    <div class="card-body">
                        <h5 class="card-title"><a href="posts/{{$post->id}}">{{$post->title}}</a></h5>
                        {{-- <p class="card-text">{!! \Illuminate\Support\Str::words(strip_tags(html_entity_decode(utf8_decode($post->body))), 20) !!}</p> --}}
                        @php
                            $body = html_entity_decode(utf8_decode($post->body));
                            $summary = strstr($body, '<!-- pagebreak -->', true);
                        @endphp
                            {!! strip_tags(html_entity_decode($summary), '<p> <div>') !!}
                        <small class="card-text">By: {{$post->user->name}}</small>
                        <p><small class="card-text">Created On: {{$post->created_at}}</small></p>
                        <a href="/{{$post->id}}" class="btn btn-primary">View Post</a>
                    </div>
                </div>
            </div>
            @endforeach
        </div>
        @else
        <h3>There is no post at the moment.</h3>
        @endif
        Total number of posts: {{$posts->total()}}    
    </div>
</section>
<div class="row">
    <div class="col-12 d-flex justify-content-center">
        {{$posts->links()}}
    </div>
</div>
@endsection

@section('footer')
    @include('layouts.footer')
@endsection
