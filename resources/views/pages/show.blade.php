@extends('layouts.app')

@section('content') 
<header id="section-header-update">

</header>
    
<section id="section-update-1">
    <div class="container">
        <div class="row">
            <div class="col-12 d-flex justify-content-center">
                <div class="card" style="width:50rem;">
                    <img class="card-img-top" src="{{asset('images/' . $post->image)}}" alt="Card image cap">
                    <div class="card-body">
                        <h5 class="card-title">{{$post->title}}</h5>
                        <p class="card-text">{!! html_entity_decode($post->body) !!}</p>
                        <small class="card-text">By: {{$post->user->name}}</small>
                        <p><small class="card-text">Created On: {{$post->created_at}}</small></p>
                        <a href="{{URL::previous()}}" class="btn btn-primary">Back</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

@endsection
