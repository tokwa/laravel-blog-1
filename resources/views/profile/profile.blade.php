@extends('layouts.app')

@section('content')
<div class="container-fluid">
    <div class="row">
        <div class="col-lg-6">
            <div class="row d-flex justify-content-center">
                <div class="col-6">
                    <form method="POST" action="{{route('profile.update', $user->id)}}" enctype="multipart/form-data">
                        {{method_field('PUT')}}
                        {{ csrf_field() }}
                        <div class="col-lg-12 d-flex justify-content-center mb-3">
                            <img src="{{asset('image/' . $user->image)}}" alt="Profile Image" id="profile-img">
                        </div>
                        <div class="col-lg-12 d-flex justify-content-center">
                            <h3>{{$user->username}}</h3>
                        </div>
                        <div class="form-group">
                            <label for="name">Choose new image:</label>
                            <input type="file" name="image" id="image"/>
                        </div>
                        <div class="form-group">
                            <label for="name">Name</label>
                            <input type="text" class="form-control {{ $errors->has('name') ? 'is-invalid' : '' }}" id="name"
                                name="name" aria-describedby="" value="{{$user->name}}">
                            @if ($errors->has('name'))
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $errors->first('name') }}</strong>
                            </span>
                            @endif
                        </div>
                        <div class="form-group">
                            <label for="Email">Email</label>
                            <input type="email" class="form-control {{ $errors->has('email') ? 'is-invalid' : '' }}" id="email"
                                name="email" aria-describedby="" value="{{$user->email}}">
                            @if ($errors->has('email'))
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $errors->first('email') }}</strong>
                            </span>
                            @endif
                        </div>
                        <button type="submit" class="btn btn-primary">Submit</button>
                    </form>
                </div>
            </div>
        </div>
        <div class="col-lg-6">
            <div class="row">
                @foreach($user->posts as $post)
                <div class="col-xl-6 mb-4">
                    <div class="card" style="width: 100%;">
                        <img class="card-img-top p-img-container" src="{{asset('images/' . $post->image)}}" alt="Card image cap">
                        <div class="card-body">
                            <h5 class="card-title">{{$post->title}}</h5>
                            <small>By: {{$user->username}}</small>
                            <br>
                            <small>Created at: {{$post->created_at}}</small>
                            <br>
                            <small>Status: <strong>
                                @if ($post->status == 1)
                                    <span class="text-danger">{{'rejected'}}</span>
                                @elseif ($post->status == 2)
                                    <span class="text-warning">{{'pending'}}</span>
                                @elseif ($post->status == 3)
                                <span class="text-primary">{{'active'}}</span>    
                                @endif
                                            </strong>
                            </small>
                            <br>
                            <br>
                            {{-- <p>{!! \Illuminate\Support\Str::words((html_entity_decode(utf8_decode($post->body))), 30) !!}</p> --}}

                            @php
                                $body = html_entity_decode(utf8_decode($post->body));
                                $summary = strstr($body, '<!-- pagebreak -->', true);
                            @endphp
                            <div>
                                {{-- {!! dd($summary) !!} --}}
                                {{-- {!! \Illuminate\Support\Str::words(strip_tags(html_entity_decode(utf8_decode($i->body)), '<p>'), 12) !!} --}}
                                {!! strip_tags(html_entity_decode($summary), '<p> <div>') !!}
                            </div>
                            <a href="/posts/{{$post->id}}" class="btn btn-primary">View Post</a>
                            <a href="/posts/{{$post->id}}/edit" class="btn btn-primary">Edit</a>
                            <!-- Button trigger modal -->
                            <button type="button" class="btn btn-danger" data-toggle="modal" data-target="#delete_{{ $post->id }}">
                                Delete
                            </button>
                            <!-- Modal -->
                            <div class="modal fade" id="delete_{{ $post->id }}" tabindex="-1" role="dialog"
                                aria-labelledby="exampleModalLabel" aria-hidden="true">
                                <div class="modal-dialog" role="document">
                                    <div class="modal-content">
                                        <div class="modal-header">
                                            <h5 class="modal-title" id="exampleModalLabel">Do you really want to delete
                                                this item?</h5>
                                        </div>
                                        <div class="modal-footer">
                                            <form method="POST" action="{{ route('posts.destroy', $post->id) }}">
                                                @method('DELETE')
                                                @csrf
                                                <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancel</button>
                                                <button type="submit" class="btn btn-danger">Delete</button>
                                            </form>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                @endforeach
                {{-- @foreach($user->posts as $post)
                    {{$post->links()}}
                @endforeach
                gsdfg --}}
            </div>
        </div>
    </div>
</div>
</div>
@endsection

@section('footer')
    @include('layouts.footer')
@endsection


{{-- <div class="card-body text-center d-flex justify-content-center flex-column">
        <h5 class="card-title">
            <strong>{{ utf8_decode($i->title) }}</strong>
        </h5>
        <p>
            {!! \Illuminate\Support\Str::words(strip_tags(html_entity_decode(utf8_decode($i->body))), 30) !!}
        </p>
        <p class="text-center">
            @if(strlen( html_entity_decode(utf8_decode($i->body)) ) > 20)
            <a class="btn btn-danger" href="{{ route('bonus', $i->slug) }}">더 보기</a>
            @endif
        </p>
    </div> --}}
