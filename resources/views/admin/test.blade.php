@extends('layouts.app')

@section('content')
   <div class="container">
        <h1>Test Page</h1>
   </div>

   @if(count($users))
        @foreach($users as $user)
            <p>
                {{$user->name}}
                <br>
                {{-- <small>By: {{$user->posts}}</small> --}}
                @foreach( $user->posts as $i )
                    <small>{{ $i->title }}</small>
                    <p>
                        {!! $i->body !!}
                    </p>
                @endforeach
            </p>
            {{-- <ul>
                @foreach($users as $user)
                    {{$user->posts}}
                @endforeach
            </ul> --}}
        @endforeach
   @else
    <p>There is no user at the moment.</p>
   @endif

@endsection