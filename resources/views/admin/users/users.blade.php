@extends('layouts.admin-layout')
@section('breadcrumb-items')
    <li>
        <span>
            Users
        </span>
    </li>
@endsection

@section('admin-content')
<section id="section-admin-user-1">
    <div class="row">
        <div class="col-lg-12 mt-5">
            <div class="card">
                <div class="card-body">
                    <h4 class="header-title">Data Table Dark</h4>
                    <div class="table-responsive data-tables datatable-dark">
                        <table class="table" id="dataTable3" class="text-center">
                            <thead class="text-capitalize">
                                <tr>
                                    <th>#</th>
                                    <th>ROLE</th>
                                    <th>USERNAME</th>
                                    <th>NAME</th>
                                    <th>EMAIL</th>
                                    <th>VERIFIED AT</th>
                                    <th>Posts</th>
                                    <th>STATUS</th>
                                    <th>EDIT</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach($users as $user)
                                <tr>
                                    <td>{{$x++}}</td>
                                    <td>
                                        @if ($user->role_id == 1)
                                            {{'user'}}
                                        @elseif ($user->role_id == 2)
                                            {{'moderator'}}
                                        @elseif ($user->role_id == 3)
                                            {{'author'}}
                                        @elseif ($user->role_id == 4)
                                            {{'admin'}}
                                        @elseif ($user->role_id == 5)
                                            <span class="text-primary">{{'superadmin'}}</span>
                                        @else
                                            {{'invalid role'}}
                                        @endif
                                    </td>
                                    <td>{{$user->username}}</td>
                                    <td>{{$user->name}}</td>
                                    <td>{{$user->email}}</td>
                                    <td>{{$user->email_verified_at}}</td>
                                    <td>{{count($user->posts)}}</td>
                                    {{-- <td>{{ !in_array($user->role_id, [1,2]) ? count($user->posts) : "N/A" }}</td> --}}
                                    <td>
                                        @if($user->is_active == 1)
                                        <span class="text-primary">{{'active'}}</span>
                                        @elseif($user->is_active == 0)
                                        <span class="text-warning">{{'inactive'}}</span>
                                        @else
                                        <span class="text-danger">{{'invalid status'}}</span>
                                        @endif
                                    </td>
                                    <td><a href="{{route('admin.edituser', $user->id)}}"><i class="fas fa-user-edit"></i></a></td>
                                </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
            <br><h6>Total number of users: {{\App\User::count()}}</h6>
        </div>
    </div>
</section>

{{-- <script src="{{asset('js/vendor/jquery-2.2.4.min.js')}}"></script>
<!-- bootstrap 4 js -->
<script src="{{asset('js/popper.min.js')}}"></script>
<script src="{{asset('js/bootstrap.min.js')}}"></script>
<script src="{{asset('js/owl.carousel.min.js')}}"></script>
<script src="{{asset('js/metisMenu.min.js')}}"></script>
<script src="{{asset('js/jquery.slimscroll.min.js')}}"></script>
<script src="{{asset('js/jquery.slicknav.min.js')}}"></script>
<!-- Start datatable js -->
<script src="https://cdn.datatables.net/1.10.19/js/jquery.dataTables.js"></script>
<script src="https://cdn.datatables.net/1.10.18/js/jquery.dataTables.min.js"></script>
<script src="https://cdn.datatables.net/1.10.18/js/dataTables.bootstrap4.min.js"></script>
<script src="https://cdn.datatables.net/responsive/2.2.3/js/dataTables.responsive.min.js"></script>
<script src="https://cdn.datatables.net/responsive/2.2.3/js/responsive.bootstrap.min.js"></script>
<!-- others plugins -->
<script src="{{asset('js/plugins.js')}}"></script>
<script src="{{asset('js/scripts.js')}}"></script> --}}

@endsection