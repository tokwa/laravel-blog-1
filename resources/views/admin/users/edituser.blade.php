@extends('layouts.admin-layout')
@section('breadcrumb-items')
    <li>
        <span>
            Edit User
        </span>
    </li>
@endsection

@section('admin-content')
<section id="section-admin-edituser-1">
    <div class="container">
        <div class="card mt-5" style="width: ;">
            <div class="card-body">
                <form method="POST" action="{{route('admin.updateuser', $user->id)}}">
                    {{method_field('PUT')}}
                    {{ csrf_field() }}
                    <div class="form-group">
                        <label for="exampleInputEmail1">Email address</label>
                        <input type="email" class="form-control {{ $errors->has('email') ? 'is-invalid' : '' }}" id="exampleInputEmail1" aria-describedby="emailHelp" name="email"
                            value="{{$user->email}}">
                        <small id="emailHelp" class="form-text text-muted">We'll never share your email with anyone
                            else.</small>
                            @if ($errors->has('email'))
                                <span class="invalid-feedback" role="alert">
                                <strong>{{ $errors->first('email') }}</strong>
                                </span>
                            @endif
                    </div>
                    <div class="form-group">
                        <label for="Username">Username</label>
                        <input type="text" class="form-control {{ $errors->has('username') ? 'is-invalid' : '' }}" id="username" name="username" value="{{$user->username}}">
                            @if ($errors->has('username'))
                                <span class="invalid-feedback" role="alert">
                                <strong>{{ $errors->first('username') }}</strong>
                                </span>
                            @endif
                    </div>
                    <div class="form-group">
                        <label for="Name">Name</label>
                        <input type="text" class="form-control {{ $errors->has('name') ? 'is-invalid' : '' }}" id="name" name="name" value="{{$user->name}}">
                            @if ($errors->has('name'))
                                <span class="invalid-feedback" role="alert">
                                <strong>{{ $errors->first('name') }}</strong>
                                </span>
                            @endif
                    </div>
                    {{-- <div class="form-group form-check">
                        <input type="checkbox" class="form-check-input" id="exampleCheck1">
                        <label class="form-check-label" for="exampleCheck1">Check me out</label>
                    </div> --}}
                    <div class="row">
                        <div class="col-lg-6">
                            <div class="form-group">
                                {{-- <b class="text-muted mb-3 mt-4 d-block">Status:</b> --}}
                                <div class="custom-control custom-radio custom-control-inline">
                                    <input type="radio" id="active" name="is_active" class="custom-control-input
                                            {{ $errors->has('is_active') ? 'is-invalid' : '' }}"
                                        value="1" {{ $user->is_active == 1 ? "checked" : "" }}>
                                    <label class="custom-control-label" for="active">Active</label>
                                </div>
                                <div class="custom-control custom-radio custom-control-inline">
                                    <input type="radio" id="hidden" name="is_active" class="custom-control-input
                                            {{ $errors->has('is_active') ? 'is-invalid' : '' }}"
                                        value="0" {{ $user->is_active == 0 ? "checked" : "" }}>
                                    <label class="custom-control-label" for="hidden">Disabled</label>
                                </div>

                                @if( $errors->has('is_active') )
                                <small class="form-text text-danger">
                                    <strong>{{ $errors->first('is_active') }}</strong>
                                </small>
                                @endif
                            </div>
                        </div>
                        <div class="col-lg-6">
                            <div class="input-group mb-3">
                                 <select class="custom-select" name="role" class="custom-control-input
                                 {{ $errors->has('role') ? 'is-invalid' : '' }}" id="inputGroupSelect02">
                                    <option value="{{$user->role_id}}">
                                        @if($user->role_id == 1)
                                            {{'User'}}
                                        @elseif ($user->role_id == 2)
                                            {{'Moderator'}}
                                        @elseif ($user->role_id == 3)
                                            {{'Author'}}
                                        @elseif  ($user->role_id == 4)
                                            {{'Admin'}}
                                        @elseif  ($user->role_id == 5)
                                            <span class="text-primary">{{'Super Admin'}}</span>
                                        @endif
                                    </option>
                                    <option value="1">User</option>
                                    <option value="2">Moderator</option>
                                    <option value="3">Author</option>
                                    <option value="4">Admin</option>
                                </select>
                                <div class="input-group-append">
                                    <label class="input-group-text" for="inputGroupSelect02">User Role</label>
                                </div>
                            </div>
                            @if( $errors->has('role') )
                            <small class="form-text text-danger">
                                <strong>{{ $errors->first('role') }}</strong>
                            </small>
                            @endif
                        </div>
                    </div>
                    <div class="form-group">
                        {{-- <a class="btn btn-danger" href="{{URL::previous()}}">Cancel</a> --}}
                        <button type="submit" class="btn btn-primary">Submit</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</section>
@endsection