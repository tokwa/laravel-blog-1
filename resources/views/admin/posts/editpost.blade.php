@extends('layouts.admin-layout')
@section('breadcrumb-items')
    <li>
        <span>
            Edit Post
        </span>
    </li>
@endsection

@section('admin-content')

<section id="section-edit-1">
    <div class="container">
        <div class="container">
            <form method="POST" action="/admin/posts/edit/{{$post->id}}" enctype="multipart/form-data">
                {{method_field('PUT')}}
                {{ csrf_field() }}
                <div class="form-group">
                    <label for="title">Post Title</label>
                    <input type="text" class="form-control {{ $errors->has('title') ? 'is-invalid' : '' }}" id="title"
                        name="title" aria-describedby="title-Help" placeholder="Post title..." value="{{$post->title}}">
                    <small id="titleHelp" class="form-text text-muted">Title of the post.</small>
                    @if ($errors->has('title'))
                    <span class="invalid-feedback" role="alert">
                        <strong>{{ $errors->first('title') }}</strong>
                    </span>
                    @endif
                </div>
                <div class="form-group">
                    <label for="body">Post Body</label>
                    <textarea class="form-control my-editor {{ $errors->has('body') ? 'is-invalid' : '' }}" id="body"
                        name="body" rows="3">{!! html_entity_decode($post->body) !!}</textarea>
                    <small id="titleHelp" class="form-text text-muted">Post content.</small>
                    @if ($errors->has('body'))
                    <span class="invalid-feedback" role="alert">
                        <strong>{{ $errors->first('body') }}</strong>
                    </span>
                    @endif
                </div>
                <div class="form-group">
                    <img src="{{asset('images/' . $post->image)}}" alt="">
                    <input type="file" name="image" id="image" />
                </div>
                {{-- <div class="form-group">
                    <p> <b>Status:</b>  
                        @if ($post->status == 0)
                            <span class="text-danger">{{'inactive'}}</span>
                        @elseif ($post->status == 1)
                            <span class="text-warning">{{'pending'}}</span>
                        @else
                            <span class="text-primary">{{'active'}}</span>
                        @endif
                    </p>
                </div> --}}
                <div class="input-group mb-5" style="width: 300px;">
                        <select class="custom-select" name="status" id="inputGroupSelect02">
                           {{-- <option selected>Choose Role</option> --}}
                           <option value="{{$post->status}}">
                            @if ($post->status == 1)
                                <span class="text-danger">Rejected</span>
                            @elseif($post->status == 2)
                                <span class="text-warning">Pending</span>
                            @elseif($post->status == 3)
                            <span class="text-primary">Approved</span>
                            @else
                                <span class="text-danger">Invalid Status</span>
                            @endif
                            </option>
                            <option value="1">Reject</option>
                            <option value="2">Pending</option>
                            <option value="3">Approved</option>
                       </select>
                       <div class="input-group-append">
                           <label class="input-group-text" for="inputGroupSelect02">Post Status</label>
                        </div>
                   </div>
                <a class="btn btn-success" href="{{URL::previous()}}">Back</a>
                {{-- <button type="button" class="btn btn-danger" data-toggle="modal" data-target="#exampleModal">
                        Reject
                </button> --}}
                <!-- Modal -->
                {{-- <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
                    aria-hidden="true">
                    <div class="modal-dialog" role="document">
                        <div class="modal-content">
                            <div class="modal-header">
                                <h5 class="modal-title" id="exampleModalLabel">Modal title</h5>
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                </button>
                            </div>
                            <div class="modal-body">
                                Are you sure you want to decline this post?
                            </div>
                            <div class="modal-footer">
                                <button type="button" class="btn btn-danger" data-dismiss="modal">Cancel</button>
                                <button type="button" class="btn btn-primary">Decline</button>
                            </div>
                        </div>
                    </div>
                </div> --}}
                {{-- <a class="btn btn-success" href="#">Approve</a> --}}
                <button type="submit" class="btn btn-primary">Submit</button>
            </form>
        </div>
    </div>
</section>



<script src="//cdn.tinymce.com/4/tinymce.min.js"></script>
{{-- <textarea name="content" class="form-control my-editor">{!! old('content', $content) !!}</textarea> --}}
<script>
    var editor_config = {
        height : "500",
        path_absolute: "/",
        selector: "textarea.my-editor",
        plugins: [
            "advlist autolink lists link image charmap print preview hr anchor pagebreak",
            "searchreplace wordcount visualblocks visualchars code fullscreen",
            "insertdatetime media nonbreaking save table contextmenu directionality",
            "emoticons template paste textcolor colorpicker textpattern"
        ],
        toolbar: "insertfile undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image media | forecolor backcolor",
        relative_urls: false,
        file_browser_callback: function (field_name, url, type, win) {
            var x = window.innerWidth || document.documentElement.clientWidth || document.getElementsByTagName(
                'body')[0].clientWidth;
            var y = window.innerHeight || document.documentElement.clientHeight || document.getElementsByTagName(
                'body')[0].clientHeight;

            var cmsURL = editor_config.path_absolute + 'laravel-filemanager?field_name=' + field_name;
            if (type == 'image') {
                cmsURL = cmsURL + "&type=Images";
            } else {
                cmsURL = cmsURL + "&type=Files";
            }

            tinyMCE.activeEditor.windowManager.open({
                file: cmsURL,
                title: 'Filemanager',
                width: x * 0.8,
                height: y * 0.8,
                resizable: "yes",
                close_previous: "no"
            });
        }
    };

    tinymce.init(editor_config);
</script>
@endsection

