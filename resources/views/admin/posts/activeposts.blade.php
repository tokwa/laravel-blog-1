@extends('layouts.admin-layout')
@section('breadcrumb-items')
    <li>
        <span>
            Active Posts
        </span>
    </li>
@endsection

@section('admin-content')
<div class="col-12 mt-5">
    <div class="card">
        <div class="card-body">
            <h4 class="header-title">Data Table Primary</h4>
            <div class="table-responsive data-tables datatable-primary  text-center">
                <table class="table" id="dataTable2">
                    <thead class="text-capitalize">
                        <tr>
                            <th>Username</th>
                            <th>Email</th>
                            <th>Title</th>
                            <th>Date Created</th>
                            <th>Status</th>
                            <th>View</th>
                            <th>Edit</th>
                        </tr>
                    </thead>
                    <tbody>
                        @if(count($posts))
                        @foreach($posts as $post)
                        <tr>
                            <td>{{$post->user->username}}</td>
                            <td>{{$post->user->email}}</td>
                            <td>{{$post->title}}</td>
                            <td>{{$post->created_at->format('M-d-Y h:i:s')}}</td>
                            <td>
                                @if ($post->status == 1)
                                    <span class="text-danger">{{'rejected'}}</span>
                                @elseif ($post->status == 2)
                                    <span class="text-warning">{{'pending'}}</span>
                                @elseif ($post->status == 3)
                                <span class="text-primary">{{'active'}}</span>    
                                @endif
                            </td>
                            <td>
                                <!-- Button trigger modal -->
                                <a href="#" class="fa fa-eye" data-toggle="modal" data-target="#showPost" 
                                    data-title="{{ $post->title }}" 
                                    data-body="{{ $post->body }}"
                                    data-username="{{ $post->user->username }}"
                                    data-created_at="{{ $post->created_at }}"
                                    data-image= "{{ asset ('images/' . $post->image) }}">       
                                </a>
                            </td>
                            <td><a href="{{route('admin.editpost', $post->id)}}"><i class="fas fa-pen-square"></i></a></td>
                        </tr>
                        @endforeach
                    </tbody>
                </table>
                @else
                <p>There is no active posts at the moment.</p>
                @endif
            </div>
        </div>
    </div>
    <br>
    <h6>Total number of posts: {{ $posts->total() }}</h6>
    <br>
</div>
<!-- Modal -->
<div class="modal fade bd-example-modal-lg" id="showPost" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
    aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel" id="title"></h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <div class="card">
                    <img class="card-img-top" id="image" src="" alt="Card image cap">
                    <div class="card-body">
                        <h5 class="card-title" id="title"></h5>
                        <p class="card-text" id="body"></p>
                        <small class="card-text">By: <span id="username"></span> </small>
                        <p><small class="card-text">Created At: <span id="created_at"></span> </small></p>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>
<!-- Primary table end -->
<div class="row">
    <div class="col-12 d-flex justify-content-center">
        {{$posts->links()}}
    </div>
</div>

@push('custom-scripts')
<script>
    $(document).ready( () => {
    $('#showPost').on('show.bs.modal', function (e) {
        var btn = $(e.relatedTarget)
        var modal = $(this)
        var title = modal.find('.modal-body #title').html(btn.data('title'))
        var body = modal.find('.modal-body #body').html(btn.data('body'))
        var username = modal.find('.modal-body #username').html(btn.data('username'))
        var created_at = modal.find('.modal-body #created_at').html(btn.data('created_at'))  
        var image = modal.find('.modal-body #image').attr('src', btn.data('image'))    
    })
    });
</script>
@endpush

{{-- <script src="{{asset('js/vendor/jquery-2.2.4.min.js')}}"></script> --}}
<!-- bootstrap 4 js -->
{{-- <script src="{{asset('js/popper.min.js')}}"></script>
<script src="{{asset('js/bootstrap.min.js')}}"></script>
<script src="{{asset('js/owl.carousel.min.js')}}"></script>
<script src="{{asset('js/metisMenu.min.js')}}"></script>
<script src="{{asset('js/jquery.slimscroll.min.js')}}"></script>
<script src="{{asset('js/jquery.slicknav.min.js')}}"></script> --}}
<!-- Start datatable js -->
{{-- <script src="https://cdn.datatables.net/1.10.19/js/jquery.dataTables.js"></script>
<script src="https://cdn.datatables.net/1.10.18/js/jquery.dataTables.min.js"></script>
<script src="https://cdn.datatables.net/1.10.18/js/dataTables.bootstrap4.min.js"></script>
<script src="https://cdn.datatables.net/responsive/2.2.3/js/dataTables.responsive.min.js"></script>
<script src="https://cdn.datatables.net/responsive/2.2.3/js/responsive.bootstrap.min.js"></script> --}}
<!-- others plugins -->
{{-- <script src="{{asset('js/plugins.js')}}"></script>
<script src="{{asset('js/scripts.js')}}"></script> --}}


@endsection