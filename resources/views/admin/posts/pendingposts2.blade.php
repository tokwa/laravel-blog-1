@extends('layouts.admin-layout')

@section('admin-content')
<div class="col-12 mt-5">
    <div class="card">
        <div class="card-body">
            <h4 class="header-title">Data Table Primary</h4>
            <div class="table-responsive data-tables datatable-primary  text-center">
                <table class="table" id="dataTable2">
                    <thead class="text-capitalize">
                        <tr>
                            <th>Username</th>
                            <th>Email</th>
                            <th>Title</th>
                            <th>Date Created</th>
                            <th>Status</th>
                            <th>View</th>
                            <th>Edit</th>
                        </tr>
                    </thead>
                    <tbody>
                        @if(count($posts))
                        @foreach($posts as $post)
                        <tr>
                            <td>{{$post->user->username}}</td>
                            <td>{{$post->user->email}}</td>
                            <td>{{$post->title}}</td>
                            <td>{{$post->created_at->format('M-d-Y h:i:s')}}</td>
                            <td>@if($post->status == 0)
                                <span class="text-danger">{{'inactive'}}</span>
                                @endif
                            </td>
                            <td><a href="#" class="fa fa-eye"></a></td>
                            <td><a href="{{route('admin.editpost', $post->id)}}"><i class="fas fa-pen-square"></i></a></td>
                        </tr>
                        @endforeach
                    </tbody>
                </table>
                @else
                <p>There is no active posts at the moment.</p>
                @endif
            </div>
        </div>
    </div>
    <br>
    <h6>Total number of posts: {{ $posts->total() }}</h6>
    <br>
</div>
<!-- Primary table end -->
<div class="row">
    <div class="col-12 d-flex justify-content-center">
        {{$posts->links()}}
    </div>
</div>

{{-- <script src="{{asset('js/vendor/jquery-2.2.4.min.js')}}"></script> --}}
<!-- bootstrap 4 js -->
<script src="{{asset('js/popper.min.js')}}"></script>
<script src="{{asset('js/bootstrap.min.js')}}"></script>
<script src="{{asset('js/owl.carousel.min.js')}}"></script>
<script src="{{asset('js/metisMenu.min.js')}}"></script>
<script src="{{asset('js/jquery.slimscroll.min.js')}}"></script>
<script src="{{asset('js/jquery.slicknav.min.js')}}"></script>
<!-- Start datatable js -->
<script src="https://cdn.datatables.net/1.10.19/js/jquery.dataTables.js"></script>
<script src="https://cdn.datatables.net/1.10.18/js/jquery.dataTables.min.js"></script>
<script src="https://cdn.datatables.net/1.10.18/js/dataTables.bootstrap4.min.js"></script>
<script src="https://cdn.datatables.net/responsive/2.2.3/js/dataTables.responsive.min.js"></script>
<script src="https://cdn.datatables.net/responsive/2.2.3/js/responsive.bootstrap.min.js"></script>
<!-- others plugins -->
<script src="{{asset('js/plugins.js')}}"></script>
<script src="{{asset('js/scripts.js')}}"></script>


@endsection