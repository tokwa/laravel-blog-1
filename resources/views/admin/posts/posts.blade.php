@extends('layouts.admin-layout')
@section('breadcrumb-items')
    <li>
        <span>
            All Cards
        </span>
    </li>
@endsection

@section('admin-content')

<div class="container-fluid">
    <div class="card-area admin-card-post-container">
        <div class="row">
        @if(count($posts))
            @foreach($posts as $post)
                <div class="col-lg-3 col-md-6 mt-5">
                    <div class="card card-bordered">
                    <img class="card-img-top img-fluid admin-posts-image-container" src="{{asset('images/' . $post->image)}}" alt="image">
                        <div class="card-body">
                            <h5 class="title">{{$post->title}}</h5>
                            <p class="card-text">{!! \Illuminate\Support\Str::words(strip_tags(html_entity_decode(utf8_decode($post->body))), 10) !!}</p>
                            <small>Created by: {{$post->user->name}}</small>
                            <br>
                            <small>Created At: {{$post->created_at}}</small>
                            <br>
                            <small>Status: 
                                <strong>
                                @if ($post->status == 1)
                                    <span class="text-danger">{{'rejected'}}</span>
                                @elseif ($post->status == 2)
                                    <span class="text-warning">{{'pending'}}</span>
                                @elseif ($post->status == 3)
                                <span class="text-primary">{{'active'}}</span>    
                                @endif
                            </strong>
                            </small>
                            <br><br>
                            <a href="/{{$post->id}}" class="btn btn-primary">View Post</a>
                        </div>
                    </div>
                </div>
            @endforeach
            @else
            <p>
                There is no post at the moment.
            </p>
            @endif
        </div>
        <div>
            {{-- <br><h6>{{ \App\Post::count() }}</h6> --}}
            <br><h6>Total number of posts: {{ $posts->total() }}</h6>
        </div>
    </div>
    <div class="row justify-content-md-center">
        <div class="col-md-auto pag">
          {{$posts->links()}}
        </div>
    </div>
</div>
    
@endsection