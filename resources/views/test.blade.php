@extends('layouts.app')
@section('title', 'Boast Forum')

@section('csrf')
<!-- CSRF Token -->
<meta name="csrf-token" content="{{ csrf_token() }}">
@endsection

@section('base-seo')
    <!-- Eggckr SEO -->
    <meta name="description" content="회원님들의 빅윈 & 잭팟 직접 스크린샷 찍어서 자랑해 주세요 !! 뽐내기에 업로드시 쿠폰도 지급해 드립니다 ^^" />
    <meta name="keywords" content="에그벳,에그카지노,공식 블로그,주소,해외,스포츠북,바카라,블랙잭,룰렛,슬롯,릴게임,무료,쿠폰,이벤트,우리카지노,엠카지노,맥스카지노,유로카지노,먹튀,검증,안전사이트,하는방법,팁,규정,노하우,강친닷컴,네임드,사다리,달팽이 ,온카,슬롯게임 리뷰," />
    <meta name="copyright" content="{{ config('app.name') }}" />
    <meta name="author" content="{{ config('app.name') }}" />
    <link rel="canonical" href="{{ URL::current() }}" />
    <meta name="application-name" content="{{ config('app.name') }}" />
    {{-- Facebook SEO --}}
    <meta property="og:site_name" content="{{ config('app.name') }}" />
    <meta property="og:title" content="{{ config('app.name') }}" />
    <meta property="og:description" content="회원님들의 빅윈 & 잭팟 직접 스크린샷 찍어서 자랑해 주세요 !! 뽐내기에 업로드시 쿠폰도 지급해 드립니다 ^^">
    <meta property="og:type" content="forum">
    <meta property="og:url" content="{{ URL::current() }}">
    <meta property="og:image" content="{{ asset('sliders/bg-forum.png') }}">
    {{-- Twitter seo --}}
    <meta name="twitter:card" content="summary_large_image">
    <meta name="twitter:site" content="@Eggckr">
    <meta name="twitter:title" content="{{ config('app.name') }}">
    <meta name="twitter:description" content="회원님들의 빅윈 & 잭팟 직접 스크린샷 찍어서 자랑해 주세요 !! 뽐내기에 업로드시 쿠폰도 지급해 드립니다 ^^">
    <meta name="twitter:image" content="{{ asset('sliders/bg-forum.png') }}">
@endsection

@section('header-styles')
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.6.3/css/all.css" integrity="sha384-UHRtZLI+pbxtHCWp1t77Bi1L4ZtiqrqD80Kn4Z8NTSRyMA2Fd33n5dQ8lWUE00s/"
        crossorigin="anonymous">
    <link rel="stylesheet" href="{{ asset('css/font-awesome.min.css') }}">
    <link rel="stylesheet" href="{{ asset('css/datetime-picker.css') }}">
@endsection

@section('aside')
@include('layouts.aside')
@endsection

@section('content')

<header>
    <img class="card-img-top banner-mobile-view" src="{{ asset('sliders/bg-forum.png') }}" alt="FORUM BACKGROUND">
    <div class="parallax-window" data-parallax="scroll" data-image-src="{{ asset('images/background/FORUM-HEADER.png') }}"></div>
</header>

<section id="page-description">
    <div class="container text-center mt-5 text-muted">
        @include('layouts.repetetives.forum')
    </div>
</section>

<section id="forum-heading" style="margin-top: 115px;">
    <div class="container-fluid hbt">
        <div class="row justify-content-center align-items-center text-center">
            <div class="col-lg-12">
                <div class="row {{ Auth::check() ? 'flex-row justify-content-center' : '' }} d-flex">
                    <div class="col-lg-4 col-md-6 col-sm-6 col mb-2 order-lg-1 order-md-2 order-sm-2 order-2">
                        <div class="dropdown d-flex justify-content-center">
                            <button class="btn dropdown-toggle btn-color btn-size" type="button" id="" data-toggle="dropdown"
                                aria-haspopup="true" aria-expanded="false" style="color: white; text-align: center;">
                                승리액<img src="{{ asset('images/bonus/arrow-down-white.png') }}" alt="" style="height: 22px; width: 22px;">
                            </button>
                            <div class="dropdown-menu text-center dropdown-hover-color" aria-labelledby="dropdownMenuButton">
                                <a class="dropdown-item" href="{{ route('forum') }}">전체보기</a>
                                <a class="dropdown-item" href="{{ route('forum.search', 'highest-to-lowest') }}">최고
                                    당첨액순</a>
                                <a class="dropdown-item" href="{{ route('forum.search', 'lowest-to-highest') }}">최저
                                    당첨액순</a>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-4 col-md-6 col-sm-6 col mb-2 order-lg-2 order-md-3 order-sm-3 order-3">
                        <div class="dropdown d-flex justify-content-lg-center justify-content-center">
                            <button class="btn dropdown-toggle btn-color btn-size" type="button" id="sortProvider"
                                data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" style="color: white; text-align: center;">
                                플랫폼<img src="{{ asset('images/bonus/arrow-down-white.png') }}" alt="" style="height: 22px; width: 22px;">
                            </button>
                            {{-- Sort By Provider ... --}}
                            <div class="dropdown-menu text-center dropdown-hover-color" aria-labelledby="sortProvider">
                                <a class="dropdown-item" href="{{ route('forum') }}">전체보기</a>
                                {{-- All --}}
                                @foreach( $gp as $g )
                                <a class="dropdown-item" href="{{ route('forum.search', $g->name) }}">{{
                                    strtoupper($g->name) }}</a>
                                @endforeach
                            </div>
                        </div>
                    </div>
                    @auth
                    @can( 'fully-verified' )
                    <div class="col-lg-4 col-md-12 col-sm-12 mb-2 order-lg-3 order-md-1 order-sm-1 order-1">
                        <div class="row justify-content-center">
                            <div class="col-md-12">
                                <div class="input-group mb-3 bs-forum-upload">
                                    <button type="button" class="btn btn-danger" data-toggle="modal" data-target="#openUpload">
                                        <h2>업로드</h2>
                                    </button>
                                </div>
                            </div>
                        </div>
                    </div>
                    @endcan
                    @endauth
                </div>
            </div>
        </div>
    </div>
</section>

<hr>

<section id="forum-body">
    <div class="container-fluid hbt">
        <div class="row">

            @if( session('success') )
            <div class="col-lg-12 mt-3">
                <div class="alert alert-success alert-dismissible fade show py-3" role="alert">
                    <strong>성공!</strong> {{ session('success') }}
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span class="fa fa-times"></span>
                    </button>
                </div>
            </div>
            @endif

            @if( !count($items) )
            <div class="col-md-12 text-center mb-5 mt-5">
                <h1 class="text-center">찾을 수 없습니다.</h1>
            </div>
            @else
            @foreach( $items as $item )
            <div class="col-lg-4 py-4">
                <div class="card">
                    <div class="card-header" style="position:relative">
                        <a href="#" data-toggle="modal" data-target="#openImagePrev" 
                        data-image="{{ asset('forum-images/'. $item->image) }}"
                        data-win_datetime="{{ $item->win_datetime }}"
                        data-game="{{ utf8_decode($item->game_info) }}"
                        data-coin="{{ number_format($item->coin) }}"
                        data-alloc="{{ number_format($item->allocation) }}"
                        data-bet="{{ number_format($item->bet) }}"
                        data-win="{{ number_format($item->win) }}">
                        <img class="card-img-top bf-img-container" src="{{ asset('forum-images/'. $item->image) }}"
                            alt="{{ str_replace('_', '', $item->image) }}" style="height:220px;object-fit:cover;object-position:50% 50%">
                        </a>
                    </div>
                    <div class="card-body" style="background: ;">
                        <div class="row forum-align-text">
                            <div class="col-xl-6 col-lg-6 col-md d-flex flex-column">
                                <strong>{{ $item->win_datetime }}</strong>
                                <strong>{{ $item->game_provider->name }}</strong>
                                <strong>{{ utf8_decode(strtoupper($item->game)) }}</strong>
                            </div>
                            <div class="col-xl-6 col-lg-6 col-md d-flex flex-column">
                                <span>
                                    <strong>아이디:</strong>{{ $item->aliased_username }} <br>
                                </span>
                                <span>
                                    <strong>베팅:</strong>{{ number_format($item->bet) }}
                                </span>
                                <span>
                                    <strong>배당:</strong>{{ $item->allocation }}
                                </span>
                            </div>
                        </div>
                    </div>
                    <div class="winning-area">
                        <span>
                            <strong>
                                <h3>승리금:
                            </strong>{{ number_format($item->win) }}</h3>
                        </span>
                    </div>
                </div>
            </div>
            @endforeach

            <div class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true"
                id="openImagePrev">
                <div class="modal-dialog modal-lg mod" style="margin-top: 100px;">
                    <div class="modal-content">
                        <div class="modal-header mb-2 pt-2 pb-2" style="background: #dadada;">
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                        <div class="win-details" style="font-size: ">
                            <div class="row">
                                <div class="col-7">
                                    <div class="row text-center">
                                        <div class="col-7">
                                            <strong>날짜: </strong><span id="win_datetime"></span>
                                        </div>
                                        <div class="col-5">
                                            <strong>베팅: </strong><span id="win_bet"></span>
                                        </div>
                                    </div>
                                    <div class="row text-center">
                                        <div class="col-7">
                                            <strong>게임 명: </strong><span id="win_game"></span>
                                        </div>
                                        <div class="col-5">
                                            <strong>배당: </strong><span id="win_alloc"></span>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-5 d-flex text-center">
                                    <div class="col-12" style="margin-top: auto; margin-bottom: auto;">
                                        <strong>당첨 금액: </strong><span style="font-weight: bolder; font-size: 120%; vertical-align:;" id="win2"></span> 
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="modal-body">
                            <img class="card-img-top" id="imgPrev" src="" alt="Forum Item Image Preview">
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-lg-12 py-4">
                <div class="row justify-content-center">
                    <div class="col-lg-6 d-flex justify-content-center">
                        {{ $items->links() }}
                    </div>
                </div>
            </div>
            @endif
        </div>
    </div>
</section>

@include('layouts.upper-footer-text')

<div class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true" id="openUpload">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true" style="color:aliceblue">&times;</span>
                </button>
            </div>
            <div class="modal-body d-flex flex-column">
                <form method="POST" id="submitForumPost" enctype="multipart/form-data">
                    @csrf
                    <div class="container-fluid">
                        <div class="row justify-content-center">
                            <div class="col-lg-7 col-md-7">
                                <h4>스크린샷 업로드</h4>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-lg-7 mb-5" style="border-style:dashed; border-color: #919191">
                                <div class="row justify-content-center align-items-center text-center">
                                    <div class="col-lg-12">
                                        <img id="img-preview" src="{{ asset('images/background/upload-image.png') }}"
                                            alt="your image" width="50%" height="auto" />
                                    </div>
                                    <div class="col-lg-12">
                                        <div class="custom-file mb-3">
                                            <input type="file" class="form-control custom-file-input" id="image_upload">
                                            <label class="custom-file-label x" for="img_upload">파일을 선택하세요</label>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-5">
                                <div class="input-group mb-3">
                                    <div class="input-group-prepend">
                                        <label class="input-group-text" for="game_providers">게임사</label>
                                    </div>
                                    <select class="custom-select" id="game_provider">
                                        @foreach( $gp as $g )
                                        <option value="{{ $g->id }}">{{ $g->name }}</option>
                                        @endforeach
                                    </select>
                                </div>

                                <div class="input-group mb-3">
                                    <div class="input-group-prepend">
                                        <span class="input-group-text" id="input2">게임 명</span>
                                    </div>
                                    <input type="text" class="form-control" id="game_name" aria-describedby="input2"
                                        placeholder="Ex. 죽음의 책">
                                </div>
                                <div class="input-group mb-3" id="pubht">
                                    <div class="input-group-prepend" id="datetimepicker" data-target-input="nearest">
                                        <span class="input-group-text" id="input3">날짜</span>
                                        <input type="text" id="win_date" name="win_date" class="form-control datetimepicker-input"
                                            data-target="#datetimepicker" />
                                        <div class="input-group-append" data-target="#datetimepicker" data-toggle="datetimepicker">
                                            <div class="input-group-text"><i class="fa fa-calendar"></i></div>
                                        </div>
                                    </div>
                                </div>
                                <hr class="separation">
                                <div class="input-group mb-3">
                                    <div class="input-group-prepend">
                                        <span class="input-group-text" id="input4">베팅</span>
                                    </div>
                                    <input type="number" class="form-control comp" id="bet_amount" aria-describedby="input4"
                                        placeholder="Ex. 25000">
                                </div>
                                <div class="input-group mb-3">
                                    <div class="input-group-prepend">
                                        <span class="input-group-text" id="input5">코인</span>
                                    </div>
                                    <input type="number" class="form-control" id="coin_size" aria-describedby="input5"
                                        placeholder="Ex. 150">
                                </div>
                                <div class="input-group mb-3">
                                    <div class="input-group-prepend">
                                        <span class="input-group-text" id="input6">당첨 금액</span>
                                    </div>
                                    <input type="number" class="form-control comp" id="win_amount" aria-describedby="input6"
                                        placeholder="Ex. 1500000">
                                </div>
                                <div class="input-group mb-3">
                                    <div class="input-group-prepend">
                                        <span class="input-group-text" id="input7">승리 금액 %</span>
                                    </div>
                                    <input type="number" class="form-control disabled" id="win_percent"
                                        aria-describedby="input7" readonly>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="submit text-right">
                        <button type="submit" class="btn btn-primary eggckr-btn" id="uploadBtn">업로드</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>

@endsection

@section('footer')
@include('layouts.footer')
@endsection

@push('bottom-scripts')
<script src="{{ asset('js/onscroll-elements.js') }}"></script>
<script src="{{ asset('js/parallax.min.js') }}"></script>
<script src="{{ asset('js/forum-img-preview.js') }}"></script>
<script>
    $('#openImagePrev').on('show.bs.modal', function (e) {
        var btn = $(e.relatedTarget)
        var modal = $(this)
        var date = modal.find('.win-details #win_datetime').html(btn.data('win_datetime'))
        var game = modal.find('.win-details #win_game').html(btn.data('game'))
        var alloc = modal.find('.win-details #win_alloc').html(btn.data('alloc'))
        var coin = modal.find('.win-details #win_coin').html(btn.data('coin'))
        var bet = modal.find('.win-details #win_bet').html(btn.data('bet'))
        var win = modal.find('.win-details #win2').html(btn.data('win'))
    })
</script>
@can('fully-verified')
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.22.2/moment.min.js"></script>
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/tempusdominus-bootstrap-4/5.0.1/js/tempusdominus-bootstrap-4.min.js"></script>
<script>
    $('#datetimepicker').datetimepicker({
        format: 'MM/DD/Y HH:mm',
    });

    var reader = new FileReader()
    reader.onload = function (e) {
        $('#img-preview').attr('src', e.target.result)
        $('#img-preview').attr('width', '85%')
        $('#img-preview').attr('height', 'auto')
    }

    function readURL(input) {
        if (input.files && input.files[0]) {
            reader.readAsDataURL(input.files[0])
        }
    }

    $("#image_upload").change(function () {
        readURL(this);
    })

    $('input[type=file]').on('change', () => {
        let x = $('input[type=file]')[0].files[0].name
        $('.x').text(x)
    })

    var b = document.getElementById('bet_amount')
    var w = document.getElementById('win_amount')
    var a = document.getElementById('win_percent')
    var comp = document.getElementsByClassName('comp')

    for (let i = 0; i < comp.length; i++) {
        comp[i].onkeyup = () => {
            a.value = 0
            if (!w.value || !b.value) {
                a.value = 0
            }
            res = parseInt(w.value) / parseInt(b.value)
            a.value = Math.round(res)
        }
    }

    document.getElementById('submitForumPost').addEventListener('submit', (e) => {
        e.preventDefault()

        $('#uploadBtn').html('<i class="fas fa-spinner fa-spin"></i> Uploading ..')
        $('#uploadBtn').prop('disabled', true)

        const x = new FormData()
        x.append('game_provider', document.getElementById('game_provider').value)
        x.append('game_name', document.getElementById('game_name').value)
        x.append('win_date', document.getElementById('win_date').value)
        x.append('bet_amount', document.getElementById('bet_amount').value)
        x.append('coin_size', document.getElementById('coin_size').value)
        x.append('win_amount', document.getElementById('win_amount').value)
        x.append('win_percent', document.getElementById('win_percent').value)
        x.append('image_upload', document.getElementById('image_upload').files[0])

        axios.post("{{ route('forum.submit') }}", x)
            .then((response) => {
                window.location.reload()
            })
            .catch((error) => {
                $('#uploadBtn').html('업로드')
                $('#uploadBtn').prop('disabled', false)
                console.log(error.response)
                const createError = error.response.data.errors

                const createFirstItem = Object.keys(createError)[0]
                const createFirstItemDOM = document.getElementById(createFirstItem)

                const createFirstErrorSms = createError[createFirstItem][0]
                const createErrSms = document.querySelectorAll('.invalid-feedback')
                Array.from(createErrSms).forEach((el) => el.remove())

                createFirstItemDOM.insertAdjacentHTML('afterend',
                    `<div class="invalid-feedback">${createFirstErrorSms}</div>`)
                const createClearErrBorder = document.querySelectorAll('.form-control')
                Array.from(createClearErrBorder).forEach((el) => el.classList.remove('is-invalid'))
                createFirstItemDOM.classList.add('is-invalid')
            })
    });
</script>
@endcan
@endpush
