<?php

namespace App\Providers;

use Auth;
use App\User;
use App\Post;
use App\Policies\UserPolicy;
use App\Policies\PostPolicy;
use Illuminate\Support\Facades\Gate;
use Illuminate\Foundation\Support\Providers\AuthServiceProvider as ServiceProvider;

class AuthServiceProvider extends ServiceProvider
{
    /**
     * The policy mappings for the application.
     *
     * @var array
     */
    protected $policies = [
        'App\Model' => 'App\Policies\ModelPolicy',
         User::class => UserPolicy::class,
         Post::class => PostPolicy::class,
    ];

    /**
     * Register any authentication / authorization services.
     *
     * @return void
     */
    public function boot()
    {
        $this->registerPolicies();

        // Gate::define('admin.edituser', function ($user) {
        //     return $user->role_id == 5;
        // });

        // Gate::define('admin.editpost', function ($user) {
        //     return $user->role_id >= 3 && $user->role_id <= 5;
        // }); 

        Gate::define('admin.edituser', 'App\Policies\UserPolicy@admin_only');

        Gate::define('admin.editpost', 'App\Policies\PostPolicy@admin_only'); 
    }
}
