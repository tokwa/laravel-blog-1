<?php

namespace App\Http\Middleware;

use Auth;
use Closure;

class ChkIfActive
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if (Auth::check() && Auth::user()->is_active == 0) {

            return redirect()->route('pages.inactive');
        }
        
            // if (!Auth::check()) {
            //     return redirect()->route('pages.index');
            // }
            
            return $next($request);
    }
}
