<?php

namespace App\Http\Middleware;
use Auth;
use Closure;

class ChkIfAdmin
{
    public function handle($request, Closure $next)
    {

        if (Auth::check() && Auth::user()->role_id >=2 && Auth::user()->role_id <=5) {
            
            return $next($request);
        }
        
        return redirect(route('pages.index'));
    }
}
