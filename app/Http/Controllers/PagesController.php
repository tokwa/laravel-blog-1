<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Post;
use App\User;
use Auth;

class PagesController extends Controller
{
    public function index(Post $posts) {

        //$posts = Post::orderBy('created_at', 'desc')->paginate(9);

        $posts = Post::where('status', '3', $posts)->orderBy('created_at', 'desc')->paginate(9);

        return view('pages.index', compact('posts'));
    }

    public function profile($username) {

        if ($username != Auth::user()->username) {

            abort(404);
        } else {
            
            return view('pages.profile', [
                'username' => User::where('username', $username)->firstOrFail()
            ]);
            }
        }


    public function show($id) {

        return view('pages.show', [
            'post' => Post::findOrFail($id)
        ]);
    }

    public function update(Request $request, User $users) {

        $request->validate([
            'name' => ['required'],
            'email' => ['required']
        ]);

        $users->update([
            'name' => $request->name,
            'email' => $request->email
        ]);

        if ($users) {
            echo 'success!';
        } else {
            echo 'failed!';
        }
    }

    public function inactive() {

        return view('pages.inactive');
    }
}
