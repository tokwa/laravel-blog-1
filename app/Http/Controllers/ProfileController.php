<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Post;
use App\User;
use Auth;

class ProfileController extends Controller
{
       
        public function profile($user) {

        $user = User::where('id', auth()->id())->findOrFail($user);
        
        return view('profile.profile', compact('user'));
    }

        public function update(Request $request, User $user) {

        $request->validate([
            'name'=> ['required'],
            'email'=> ['required', 'email', 'unique:users,email,' . $user->id],
        ]);

        if ($request->hasFile('image')) {
            $imgName = date('Y-m-d'). "_" . 
            $request->file('image')->getClientOriginalName();
            $user->image = $imgName;
            $request->image->move(public_path('image'), $imgName);
        }

        $user->update(request([
            'name',
            'email',
            'image'
        ]));


        return redirect()->route('profile', $user->id)->with('success', 'Success!');
    }

}

