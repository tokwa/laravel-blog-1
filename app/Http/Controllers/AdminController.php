<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Gate;
use Illuminate\Http\Request;
use App\User;
use App\Post;
use Auth;

class AdminController extends Controller
{

    public function __construct() {

        $this->middleware('auth')->except(['test']);
     }
    
    public function index() {

        return view('admin.index');
    }


    // POSTS METHODS

    public function posts(Post $posts) {

        $posts = Post::orderBy('created_at', 'desc')->paginate(8);

        return view ('admin.posts.posts', compact('posts'));
    }

    public function allposts(Post $posts) {

        $posts = Post::orderBy('created_at', 'desc')->paginate(10);

        return view('admin.posts.allposts', compact('posts'));
    }

    public function activeposts(Post $posts) {

        $posts = Post::where('status', '3', $posts)->orderBy('created_at', 'desc')->paginate(10);

        return view('admin.posts.activeposts', compact('posts'));
    }

    public function pendingposts(Post $posts) {

        $posts = Post::where('status', '2', $posts)->paginate(10);

        return view('admin.posts.pendingposts', compact('posts'));
    }

    public function rejectedposts(Post $posts) {

        $posts = Post::where('status', '1', $posts)->orderBy('created_at', 'desc')->paginate(10);

        return view('admin.posts.rejectedposts', compact('posts'));
    }

    public function editpost(Post $post) {

        if (Gate::denies('admin.editpost')) {
            return back()->with('error', 'Access to this page is denied.');     
        }

        return view('admin.posts.editpost', compact('post'));
    }

    public function updatepost(Request $request, Post $post) {

        $user = User::select('role_id')->where('id', Auth()->id())->firstOrFail();

        $request->validate([
            'title' => ['required'],
            'body' => ['required'],
            'status' => ['required',]
        ]);

        $post->status = $request->input('status');

        if ( [$user, 'exists:roles,id']) {
            
            $post->update(request([
                'title',
                'body',
                'status'
            ]));
            
            return redirect()->route('admin.editpost', $post->id)->with('success', 'Post ' . $post->title . ' has been updated!');

            } else {
            return redirect()->route('admin.editpost', $post->id)->with('error', 'Failed to update ' . $post->title . '!');
        }

    }
    
    
    // USER METHODS

    public function users() {

        $users = User::get();
        $x = 1;

        return view('admin.users.users', compact('users', 'x'));
    }

    public function edit(User $user) {

        if (Gate::denies('admin.edituser', $user)) {
            return redirect()->route('admin.users')->with('error', 'Access to this page is denied.');
        }

        return view('admin.users.edituser', compact('user'));
    }

    public function update(Request $request, $id) {

        $user = User::where('id', $id)->findOrFail($id);

        $request->validate([
            'email'=> ['required', 'email', 'unique:users,email,' . $id],
            'username'=> ['required', 'unique:users,username,' . $id],
            'name'=> ['required'],
            'role' => ['exists:roles,id', 'in:1,2,3,4'],
            'is_active' => ['exists:users,is_active', 'in:1,0']
        ]);

        $user->role_id = $request->input('role');
        $user->is_active = $request->input('is_active');
        
            $user->update(request([
                'email',
                'username',
                'name',
                'role_id',
                'is_active',
            ]));
        
        if ($user) {
            
            return redirect()->route('admin.users')->with('success', 'User ' . $user->username . ' has been updated!');

            } else {

            return redirect()->route('admin.users')->with('error', 'Failed to update ' . $user->username . '!');
        }

    }

    // CATEGORY METHODS

    public function cretecategory() {

        return view('admin.category.create');
    }

    public function test() {

        $users = User::orderBy('created_at', 'desc')->paginate(20);

        return view('admin.test', compact('users'));
    }

}
