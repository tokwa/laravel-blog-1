<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Post;
use App\User;
use App\Category;
use Auth;

class PostController extends Controller
{
   
    public function __construct() {

        $this->middleware(['auth', 'verified', 'chkIfActive'])->except(['index', 'show']);

     }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $posts = Post::where('user_id', auth()->id());
        
        return view('posts.index', compact('posts'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Category $categories)
    {
        $categories = Category::orderBy('created_at', 'desc')->get();

        return view('posts.create', compact('categories'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        $request->validate([
            'title'=>['required', 'string'],
            'body'=>['required'],
            'image' => ['required', 'image', 'max:2048'],
            'categories' => ['required', 'exists:categories,id']
        ]);

        $post = new Post;
        $post->title = $request->input('title');
        $post->body = ($request->input('body')); // htmlentities to escape illegal characters

        $post->user_id = Auth::user()->id;

        if ($request->hasFile('image')) {
            $imgName = date('Y-m-d'). "_" . 
            $request->file('image')->getClientOriginalName();
            $post->image = $imgName;
        }

        if ($post->save()) {
            $post->categories()->attach($request->input('categories'));
            $request->image->move(public_path('images'), $imgName);
            
            return redirect()->route('profile.profile', $post->user_id)->with('success', 'Post Successfully Created!');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Post  $post
     * @return \Illuminate\Http\Response
     */
    public function show(Post $post)
    {
        return view('posts.show', compact('post'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Post  $post
     * @return \Illuminate\Http\Response
     */
    public function edit(Post $post, User $user)
    {
        // if (auth()->id() != $post->user_id) {
        //     abort(404);
        // }
 
        if (auth()->id() == $post->user_id) {
            return view('posts.edit', compact('post'));
        } else {
            abort(404);
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Post  $post
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Post $post)
    {

        $request->validate([
            'title'=> ['required'],
            'body'=> ['required'],
            'image'
        ]);

        if ($request->hasFile('image')) {
            $imgName = date('Y-m-d'). "_" . 
            $request->file('image')->getClientOriginalName();
            $post->image = $imgName;
            $request->image->move(public_path('images'), $imgName);
        }

        $post->update(request([
            'title',
            'body',
            'image'
        ]));

        return redirect()->route('profile.profile', ['user_id' => $post->user_id])->with('success', 'Post with title ' . $post->title . ' has been updated.');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Post  $post
     * @return \Illuminate\Http\Response
     */
    public function destroy(Post $post)
    {
        if (auth()->id() != $post->user_id) {
            abort(404);
        }

        $post->delete();

        return redirect()->route('profile.profile', $post->user_id)->with('danger', 'Post with title ' . $post->title . ' has been deleted.');
        
    }
    
}

