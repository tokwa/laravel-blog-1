<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Category extends Model
{
    public function posts() {
        return $this->belongsToMany('App\Post', 'category_post'); // second argument is pivot table name
    }
}
