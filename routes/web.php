<?php


/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Route::get('/', function () {
//     return view('welcome');
// });

//Route::get('/home', 'HomeController@index')->name('home')->middleware('chkIfAdmin');

// Route::get('/admin', ['middleware' => 'chk', function () {

//     return view('admin.posts');
// }]);

// Route::get('/', 'PagesController@index', function () {
//     return view('/pages');
// });

Auth::routes(['verify' => true]);

Route::get('/home', 'PagesController@index')->name('home');

//ADMIN CONTROLLER
Route::prefix('admin')->middleware(['auth', 'chkIfAdmin', 'chkIfActive'])->group( function () {
    Route::get('test', 'AdminController@test')->name('admin.test');
    Route::get('/', 'AdminController@index')->name('admin.index');
    Route::get('posts/posts', 'AdminController@posts')->name('admin.posts');
    Route::get('posts/allposts', 'AdminController@allposts')->name('admin.allposts');
    Route::get('posts/activeposts', 'AdminController@activeposts')->name('admin.activeposts');
    Route::get('posts/pendingposts', 'AdminController@pendingposts')->name('admin.pendingposts');
    Route::get('rejectedposts', 'AdminController@rejectedposts')->name('admin.rejectedposts');
    Route::get('posts/edit/{post}', 'AdminController@editpost')->name('admin.editpost');
    Route::put('posts/edit/{post}', 'AdminController@updatepost')->name('updatepost');
    
    // USER CONTROLLER
    Route::get('users/users', 'AdminController@users')->name('admin.users');
    Route::get('users/{user}', 'AdminController@edit')->name('admin.edituser');
    Route::put('users/{id}/update', 'AdminController@update')->name('admin.updateuser');

    // CATEGORY CONTROLLER
    Route::get('category', 'AdminController@cretecategory')->name('admin.newcategory');
});

// PROFILE CONTROLLER
Route::prefix('profile')->middleware(['auth', 'verified', 'chkIfActive'])->group( function () {
    Route::get('{user}', 'ProfileController@profile')->name('profile.profile');
    Route::put('{user}', 'ProfileController@update')->name('profile.update');
}); 

// POSTS RESOURCE CONTROLLER
Route::resource('posts', 'PostController');

// PAGES CONTROLLER
Route::get('/', 'PagesController@index')->name('pages.index');
Route::get('/inactive', 'PagesController@inactive')->name('pages.inactive');
Route::get('/{id}', 'PagesController@show')->name('pages.show');


